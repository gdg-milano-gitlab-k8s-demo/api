package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetLikes(t *testing.T) {
	dbConn := os.Getenv("DB_CONN")
	if dbConn == "" {
		dbConn = "host=localhost port=5432 user=gdgmilano dbname=gdgmilano password=gdgmilano sslmode=disable"
	}
	fmt.Printf("Connecting to %v\n", dbConn)
	db, err := gorm.Open("postgres", dbConn)
	if err != nil {
		assert.Failf(t, "%v\n", err.Error())
	}
	defer db.Close()

	hostname := "testhostname"
	commitHash := "testhash"

	db.DropTableIfExists(&like{})
	db.AutoMigrate(&like{})

	router := setupRouter(db, hostname, commitHash)
	w := performRequest(router, "GET", "/")

	assert.Equal(t, http.StatusOK, w.Code)

	var response struct {
		Likes    uint   `json:"likes"`
		Commit   string `json:"commit"`
		Hostname string `json:"hostname"`
	}

	if err := json.Unmarshal(w.Body.Bytes(), &response); err != nil {
		assert.Failf(t, "%v\n", err.Error())
	}

	assert.Equal(t, uint(0), response.Likes)
	assert.Equal(t, commitHash, response.Commit)
	assert.Equal(t, hostname, response.Hostname)
}

func TestPostLike(t *testing.T) {
	dbConn := os.Getenv("DB_CONN")
	if dbConn == "" {
		dbConn = "host=localhost port=5432 user=gdgmilano dbname=gdgmilano password=gdgmilano sslmode=disable"
	}
	fmt.Printf("Connecting to %v\n", dbConn)
	db, err := gorm.Open("postgres", dbConn)
	if err != nil {
		assert.Failf(t, "%v\n", err.Error())
	}
	defer db.Close()

	hostname := "testhostname"
	commitHash := "testhash"

	db.DropTableIfExists(&like{})
	db.AutoMigrate(&like{})

	router := setupRouter(db, hostname, commitHash)
	w := performRequest(router, "POST", "/like")

	assert.Equal(t, http.StatusOK, w.Code)

	var count uint
	db.Model(&like{}).Count(&count)

	assert.Equal(t, uint(1), count)
}
