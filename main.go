package main

import (
	"fmt"
	"os"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type like struct {
	ID        int
	Timestamp time.Time
}

func main() {
	dbConn := os.Getenv("DB_CONN")
	if dbConn == "" {
		dbConn = "host=localhost port=5432 user=gdgmilano dbname=gdgmilano password=gdgmilano sslmode=disable"
	}
	fmt.Printf("Connecting to %v\n", dbConn)
	db, err := gorm.Open("postgres", dbConn)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&like{})

	hostname, _ := os.Hostname()
	commitHash := os.Getenv("COMMIT_HASH")
	if commitHash == "" {
		commitHash = "error"
	}

	r := setupRouter(db, hostname, commitHash)
	r.Run()
}

func setupRouter(db *gorm.DB, hostname, commitHash string) *gin.Engine {
	r := gin.Default()

	r.Use(cors.Default())

	r.GET("/", func(c *gin.Context) {
		var count uint
		if err := db.Model(&like{}).Count(&count).Error; err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}

		c.JSON(200, gin.H{
			"hostname": hostname,
			"commit":   commitHash,
			"likes":    count,
		})
	})

	r.POST("/like", func(c *gin.Context) {
		like := like{
			Timestamp: time.Now(),
		}
		if err := db.Create(&like).Error; err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
			return
		}
		c.JSON(200, gin.H{
			"message": "ok",
		})
	})
	return r
}
