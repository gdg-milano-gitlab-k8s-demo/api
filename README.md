# Backend Api

### [webMeetup #1] GDG Cloud Milano - Live Code: CI/CD con Gitlab-CI e Kubernetes

This app is the backend api microservice of the application developed during the [first webmeetup of GDG Cloud Milano](https://www.youtube.com/watch?v=IBNwrk24BLk).

## Start DB with docker-compose

```
docker-compose -f utils/docker-compose.yaml up
```

## Serve

In another shell, run

```
go run main.go
```
